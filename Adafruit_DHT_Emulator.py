from EmulatorGUI import GPIO
from EmulatorGUI import Sensors

DHT22 = "DHT22Sensor"

def read_retry(sensor, pin):
    if pin is None or pin != GPIO.temperaturePin:
        raise ValueError('Pin must be a valid GPIO number 0 to 31.')
    # Get a reading from C driver code.
    if sensor != DHT22:
        # Some kind of error occured.
        raise RuntimeError('Error calling DHT test driver read')
    humidity = Sensors.humidity
    temp = Sensors.temp
    return (humidity, temp)

