# cd ~
# git clone https://github.com/adafruit/Adafruit_Python_DHT.git
# cd Adafruit_Python_DHT
# sudo apt-get update
# sudo apt-get install build-essential python-dev
# sudo python setup.py install

# sudo reboot

import Adafruit_DHT as dht
h,t = dht.read_retry(dht.DHT22, 4)
print ('Temp={0:0.1f}*C  Humidity={1:0.1f}%'.format(t, h))