# RASPBERRY PI IMPORTS
#import RPi.GPIO as GPIO
#from picamera import PiCamera
#import spidev
#import smbus
#import serial
# import Adafruit_DHT as DHT

# # TESTING IMPORTS
from EmulatorGUI import GPIO
import Adafruit_DHT_Emulator as dht

# from EmulatorCamera import PiCamera
# from Emulator1Wire import W1
# from EmulatorSPI import spidev
# from EmulatorI2C import smbus
# from EmulatorSerial import serial

# OTHER IMPORTS
# import os
# import sys
import time
# import traceback


def Main():
    # try:
            
    GPIO.setmode(GPIO.BCM)

    GPIO.setwarnings(False)

    GPIO.setup(4, GPIO.IN) 
    GPIO.setup(17, GPIO.OUT) 
    GPIO.setup(18, GPIO.OUT)
    GPIO.setup(21, GPIO.OUT)
    GPIO.setup(23, GPIO.IN, pull_up_down = GPIO.PUD_UP) 
    GPIO.setup(15, GPIO.IN, pull_up_down = GPIO.PUD_DOWN) 
    GPIO.setup(24, GPIO.IN, pull_up_down = GPIO.PUD_DOWN) 
    GPIO.setup(26, GPIO.IN)

    while(True):

        GPIO.output(18, GPIO.HIGH)
        time.sleep(1)
        GPIO.output(18, GPIO.LOW)
        time.sleep(1)

        h,t = dht.read_retry(dht.DHT22, 4)
        print ('Temp=%.2f*C  Humidity=%.2f%%' % (t, h))


        if (GPIO.input(23) == False):
            GPIO.output(4, GPIO.HIGH)
            GPIO.output(17,GPIO.HIGH)
            time.sleep(1)

        if (GPIO.input(15) == True):
            GPIO.output(18,GPIO.HIGH)
            GPIO.output(21,GPIO.HIGH)
            time.sleep(1)

        if (GPIO.input(24) == True):
            GPIO.output(18,GPIO.LOW)
            GPIO.output(21,GPIO.LOW)
            time.sleep(1)
            
        if (GPIO.input(26) == True):
            GPIO.output(4,GPIO.LOW)
            GPIO.output(17,GPIO.LOW)
            time.sleep(1)



    # except Exception as ex:
    #     traceback.print_exc()
    # finally:
    #     GPIO.cleanup() #this ensures a clean exit
    


Main()
    












