# import tkinter as Tkinter
# import tkinter.messagebox as tkMessageBox

# top = Tkinter.Tk()

# def helloCallBack():
#    tkMessageBox.showinfo( "Hello Python", "Hello World")

# B = Tkinter.Button(top, text ="Hello", command = helloCallBack)

# B.pack()
# top.mainloop()

from tkinter import *
#from config_test import verb_list
import random
from config_test import *
import threading

print(sensorData)
print (random.choice(verb_list))

class App(threading.Thread):
              
	#def callback(self):
	#	self.root.quit()

	root = Tk()
	root.wm_title("GPIO EMULATOR")
	#root.protocol("WM_DELETE_WINDOW", self.callback)
	global LEDCanvas
	LEDCanvas = Canvas(root, width=105, height=50, background='white')
	LEDCanvas.grid(row = 1,column = 0)

	frame = Frame(root)
	frame.grid(row = 0, column = 0, sticky = "n")

	dictionaryPinsTkinter = {}

	pin2label = Label(text="5V", fg="red")
	pin2label.grid(row=0, column=0, padx=(10, 10))

	#5V
	pin4label = Label(text="5V", fg="red")
	pin4label.grid(row=0, column=1, padx=(10, 10))

	#GND
	pin6label = Label(text="GND", fg="black")
	pin6label.grid(row=0, column=2, padx=(10, 10))

	#GPIO14
	pin8btn = Button(text="GPIO14\nOUT=0", command="14", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin8btn.grid(row=0, column=3, padx=(10, 10),pady=(5,5))

	dictionaryPinsTkinter["14"] = pin8btn


	#GPIO15
	pin10btn = Button(text="GPIO15\nOUT=0", command="15", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin10btn.grid(row=0, column=4, padx=(10, 10))

	dictionaryPinsTkinter["15"] = pin10btn


	#GPIO18
	pin12btn = Button(text="GPIO18\nOUT=0", command="18",  padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin12btn.grid(row=0, column=5, padx=(10, 10))

	dictionaryPinsTkinter["18"] = pin12btn


	#GND
	pin14label = Label(text="GND", fg="black")
	pin14label.grid(row=0, column=6, padx=(10, 10))

	#GPIO23
	pin16btn = Button(text="GPIO23\nOUT=0", command="23", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin16btn.grid(row=0, column=7, padx=(10, 10))

	dictionaryPinsTkinter["23"] = pin16btn

	#GPIO24
	pin18btn = Button(text="GPIO24\nOUT=0",command="24",  padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin18btn.grid(row=0, column=8, padx=(10, 10))

	dictionaryPinsTkinter["24"] = pin18btn
	print("break here2")


	#GND
	pin20label = Label(text="GND", fg="black")
	pin20label.grid(row=0, column=9, padx=(10, 10))

	#GPIO25
	pin22btn = Button(text="GPIO25\nOUT=0", command="25", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin22btn.grid(row=0, column=10, padx=(10, 10))

	dictionaryPinsTkinter["25"] = pin22btn


	#GPIO08
	pin24btn = Button(text="GPIO8\nOUT=0", command="8", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin24btn.grid(row=0, column=11, padx=(10, 10))

	dictionaryPinsTkinter["8"] = pin24btn


	#GPIO07
	pin26btn = Button(text="GPIO7\nOUT=0", command="7",  padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin26btn.grid(row=0, column=12, padx=(10, 10))

	dictionaryPinsTkinter["7"] = pin26btn


	#ID_SC
	pin28label = Label(text="ID_SC", fg="black")
	pin28label.grid(row=0, column=13, padx=(10, 10))

	#GND
	pin30label = Label(text="GND", fg="black")
	pin30label.grid(row=0, column=14, padx=(10, 10))

	#GPIO12
	pin32btn = Button(text="GPIO12\nOUT=0", command="12", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin32btn.grid(row=0, column=15, padx=(10, 10))

	dictionaryPinsTkinter["12"] = pin32btn
	print("break here3")


	#GND
	pin34label = Label(text="GND", fg="black")
	pin34label.grid(row=0, column=16, padx=(10, 10))

	#GPIO16
	pin36btn = Button(text="GPIO16\nOUT=0", command="16",  padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin36btn.grid(row=0, column=17, padx=(10, 10))

	dictionaryPinsTkinter["16"] = pin36btn

	#GPIO20
	pin38btn = Button(text="GPIO20\nOUT=0", command="20", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin38btn.grid(row=0, column=18, padx=(10, 10))

	dictionaryPinsTkinter["20"] = pin38btn

	#GPIO21
	pin40btn = Button(text="GPIO21\nOUT=0", command="21", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin40btn.grid(row=0, column=19, padx=(10, 10))


	dictionaryPinsTkinter["21"] = pin40btn

	#####bottom

	#3V3
	pin1label = Label(text="3V3", fg="dark orange")
	pin1label.grid(row=1, column=0, padx=(10, 10), pady=(5,5))

	#GPIO02
	pin03btn = Button(text="GPIO2\nOUT=0",command="2", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin03btn.grid(row=1, column=1, padx=(10, 10),pady=(5,5))

	dictionaryPinsTkinter["2"] =pin03btn

	#GPIO03
	pin05btn = Button(text="GPIO3\nOUT=0", command="3", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin05btn.grid(row=1, column=2, padx=(10, 10))

	dictionaryPinsTkinter["3"] = pin05btn

	#GPIO04
	pin07btn = Button(text="GPIO4\nOUT=0", command="4", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin07btn.grid(row=1, column=3, padx=(10, 10))

	dictionaryPinsTkinter["4"] = pin07btn
	print("break here4")

	#gnd
	pin09label = Label(text="GND", fg="black")
	pin09label.grid(row=1, column=4, padx=(10, 10))

	#GPIO17
	pin11btn = Button(text="GPIO17\nOUT=0", command="17", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin11btn.grid(row=1, column=5, padx=(10, 10))

	dictionaryPinsTkinter["17"] = pin11btn

	#GPIO27
	pin13btn = Button(text="GPIO27\nOUT=0", command="27", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin13btn.grid(row=1, column=6, padx=(10, 10))

	dictionaryPinsTkinter["27"] = pin13btn

	#GPIO22
	pin15btn = Button(text="GPIO22\nOUT=0", command="22", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin15btn.grid(row=1, column=7, padx=(10, 10))

	dictionaryPinsTkinter["22"] = pin15btn

	#3V3
	pin17label = Label(text="3V3", fg="dark orange")
	pin17label.grid(row=1, column=8, padx=(10, 10))

	#GPIO10
	pin19btn = Button(text="GPIO10\nOUT=0", command="10",  padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin19btn.grid(row=1, column=9, padx=(10, 10))

	dictionaryPinsTkinter["10"] = pin19btn

	#GPIO09
	pin21btn = Button(text="GPIO9\nOUT=0", command="9", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin21btn.grid(row=1, column=10, padx=(10, 10))

	dictionaryPinsTkinter["9"] = pin21btn

	#GPIO11
	pin23btn = Button(text="GPIO11\nOUT=0", command="11", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin23btn.grid(row=1, column=11, padx=(10, 10))

	dictionaryPinsTkinter["11"] = pin23btn

	#gnd
	pin25label = Label(text="GND", fg="black")
	pin25label.grid(row=1, column=12, padx=(10, 10))

	#ID_SD
	pin27label = Label(text="ID_SD", fg="black")
	pin27label.grid(row=1, column=13, padx=(10, 10))

	#GPIO05
	pin29btn = Button(text="GPIO5\nOUT=0", command="5", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin29btn.grid(row=1, column=14, padx=(10, 10))

	dictionaryPinsTkinter["5"] = pin29btn
	print("break here5")

	#GPIO06
	pin31btn = Button(text="GPIO6\nOUT=0", command="6", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin31btn.grid(row=1, column=15, padx=(10, 10))

	dictionaryPinsTkinter["6"]=pin31btn

	#GPIO13
	pin33btn = Button(text="GPIO13\nOUT=0", command="13", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin33btn.grid(row=1, column=16, padx=(10, 10))

	dictionaryPinsTkinter["13"] = pin33btn

	#GPIO19
	pin35btn = Button(text="GPIO19\nOUT=0", command="19", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin35btn.grid(row=1, column=17, padx=(10, 10))

	dictionaryPinsTkinter["19"] = pin35btn

	    
	#GPIO26
	pin37btn = Button(text="GPIO26\nOUT=0", command="26", padx ="1px", pady="1px", bd="0px", fg="blue", relief="sunken", activeforeground="blue")
	pin37btn.grid(row=1, column=18, padx=(10, 10))


	dictionaryPinsTkinter["26"] = pin37btn

	#gnd
	pin39label = Label(text="GND", fg="black")
	pin39label.grid(row=1, column=19, padx=(10, 10))

	root.geometry('%dx%d+%d+%d' % (1300, 150, 0, 0))

	root.mainloop()


app=App()
app.start()
